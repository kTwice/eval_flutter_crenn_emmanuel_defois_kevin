class Plant {
  final int? _id;
  String _name;
  String _description;
  int _quantity;
  DateTime _date;
  String _imageUrl;

  Plant(this._id, String name, String description, int quantity, String imageUrl)
      //Image image)
      : _name = name,
        _description = description,
        _quantity = quantity,
        _imageUrl = imageUrl,
        _date = DateTime.now()
  ;

  int? get id => _id;
  String get name => _name;
  String get description => _description;
  int get quantity => _quantity;
  String get imageUrl => _imageUrl;
  DateTime get date => _date;

  set name(String? name) => _name = name!;
  set description(String? description) => _description = description!;
  set quantity(int? quantity) => _quantity = quantity!;
  set date(DateTime? date) => _date = date!;
  set imageUrl(String? imageUrl) => _imageUrl = imageUrl!;
}
