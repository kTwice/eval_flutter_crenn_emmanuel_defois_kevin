import 'package:im_plante_e/interface/plant_entity_manager.dart';
import 'package:im_plante_e/model/plant.dart';

class PlantManager {
  Plant? _currentPlant;
  List<Plant> _plants = [];
  final PlantDataManager _plantDataManager = PlantDataManager();

  Future<List<Plant>> loadPlantListFromLocal() async {
    _plants = await _plantDataManager.getPlantList();
    return _plants;
  }

  Future<Plant?> loadSinglePlantFromLocal(Plant plant) async {
    _currentPlant = await _plantDataManager.getSinglePlantById(plant.id!);
    return _currentPlant;
  }

  Future<Plant?> searchPlantByNameFromLocal(String plantName) async {
    _currentPlant = await _plantDataManager.getSinglePlantByName(plantName);
    return _currentPlant;
  }

  void addPlantToLocal(String plantName, int plantQuantity,
      String plantDescription, String imageUrl) {
    final plant =
        Plant(null, plantName, plantDescription, plantQuantity, imageUrl);
    //_plants.add(plant);
    _plantDataManager.addNewPlant(plant);
  }

  void changePlantToLocal(Plant plant, String plantName, int plantQuantity,
      String plantDescription, String imageUrl) {
    final plantToSaved = Plant(
        plant.id, plantName, plantDescription, plantQuantity, plant.imageUrl);
    _plantDataManager.updatePlantToLocal(plantToSaved);
  }

  void deletePlantToLocal(plant) {
    _plantDataManager.deleteCurrentPlant(plant);
  }

  List<Plant> get plants {
    return _plants;
  }

  Plant? get currentPlant => _currentPlant;
}
