// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'plant_entity_manager.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

// ignore: avoid_classes_with_only_static_members
class $FloorAppDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder databaseBuilder(String name) =>
      _$AppDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$AppDatabaseBuilder(null);
}

class _$AppDatabaseBuilder {
  _$AppDatabaseBuilder(this.name);

  final String? name;

  final List<Migration> _migrations = [];

  Callback? _callback;

  /// Adds migrations to the builder.
  _$AppDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$AppDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<AppDatabase> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name!)
        : ':memory:';
    final database = _$AppDatabase();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$AppDatabase extends AppDatabase {
  _$AppDatabase([StreamController<String>? listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  PlantDao? _plantDaoInstance;

  Future<sqflite.Database> open(
    String path,
    List<Migration> migrations, [
    Callback? callback,
  ]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 1,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
        await callback?.onConfigure?.call(database);
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `DBPlant` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `name` TEXT NOT NULL, `description` TEXT NOT NULL, `quantity` INTEGER NOT NULL, `publishDate` TEXT NOT NULL, `imageUrl` TEXT NOT NULL)');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  PlantDao get plantDao {
    return _plantDaoInstance ??= _$PlantDao(database, changeListener);
  }
}

class _$PlantDao extends PlantDao {
  _$PlantDao(
    this.database,
    this.changeListener,
  )   : _queryAdapter = QueryAdapter(database),
        _dBPlantInsertionAdapter = InsertionAdapter(
            database,
            'DBPlant',
            (DBPlant item) => <String, Object?>{
                  'id': item.id,
                  'name': item.name,
                  'description': item.description,
                  'quantity': item.quantity,
                  'publishDate': item.publishDate,
                  'imageUrl': item.imageUrl
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<DBPlant> _dBPlantInsertionAdapter;

  @override
  Future<List<DBPlant>> findAllPlant() async {
    return _queryAdapter.queryList('SELECT * from DBPlant',
        mapper: (Map<String, Object?> row) => DBPlant(
            row['id'] as int?,
            row['name'] as String,
            row['description'] as String,
            row['quantity'] as int,
            row['publishDate'] as String,
            row['imageUrl'] as String));
  }

  @override
  Future<DBPlant?> findPlantById(int id) async {
    return _queryAdapter.query('SELECT * from DBPlant WHERE id = ?1',
        mapper: (Map<String, Object?> row) => DBPlant(
            row['id'] as int?,
            row['name'] as String,
            row['description'] as String,
            row['quantity'] as int,
            row['publishDate'] as String,
            row['imageUrl'] as String),
        arguments: [id]);
  }

  @override
  Future<DBPlant?> findPlantByName(String name) async {
    return _queryAdapter.query('SELECT * from DBPlant WHERE name = ?1',
        mapper: (Map<String, Object?> row) => DBPlant(
            row['id'] as int?,
            row['name'] as String,
            row['description'] as String,
            row['quantity'] as int,
            row['publishDate'] as String,
            row['imageUrl'] as String),
        arguments: [name]);
  }

  @override
  Future<void> updatePlant(
    String name,
    String description,
    int quantity,
    int id,
  ) async {
    await _queryAdapter.queryNoReturn(
        'UPDATE DBPlant SET name = ?1, description = ?2, quantity = ?3 WHERE id = ?4',
        arguments: [name, description, quantity, id]);
  }

  @override
  Future<DBPlant?> deletePlant(int id) async {
    return _queryAdapter.query('DELETE from DBPlant WHERE id = ?1',
        mapper: (Map<String, Object?> row) => DBPlant(
            row['id'] as int?,
            row['name'] as String,
            row['description'] as String,
            row['quantity'] as int,
            row['publishDate'] as String,
            row['imageUrl'] as String),
        arguments: [id]);
  }

  @override
  Future<void> insertPlant(DBPlant plant) async {
    await _dBPlantInsertionAdapter.insert(plant, OnConflictStrategy.abort);
  }
}
