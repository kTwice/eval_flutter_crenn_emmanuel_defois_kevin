import 'dart:async';
import 'dart:convert';
import 'dart:io' as Io;
import 'package:flutter/widgets.dart';
import 'package:floor/floor.dart';
import 'package:flutter/material.dart';
import 'package:im_plante_e/model/plant.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

part 'plant_entity_manager.g.dart'; //the generated code will be there

// when a user send an image it's send in asset folder img and erease plant.jpg
String imagePath = '/lib/assets/img/plant.jpg';

@Database(version: 1, entities: [DBPlant])
abstract class AppDatabase extends FloorDatabase {
  PlantDao get plantDao;
}

//class which will map the return of bdd to a plantDao (use Dao object function)
class PlantDataManager {
  Future<PlantDao> get _plantDao async {
    final database =
        await $FloorAppDatabase.databaseBuilder('app_database4.db').build();

    return database.plantDao;
  }

  Future<void> addNewPlant(Plant plant) async {
    final plantDao = await _plantDao;
    return plantDao.insertPlant(DBPlant.fromMap(plant));
  }

  Future<void> updatePlantToLocal(Plant plant) async {
    final plantDao = await _plantDao;
    plantDao.updatePlant(
        plant.name, plant.description, plant.quantity, plant.id!);
  }

  Future<void> deleteCurrentPlant(Plant plant) async {
    final plantDao = await _plantDao;
    plantDao.deletePlant(plant.id!);
  }

  Future<Plant?> getSinglePlantByName(String name) async {
    final plantDao = await _plantDao;
    final dbPlant = await plantDao.findPlantByName(name);
    return dbPlant?.plant;
  }

  Future<Plant?> getSinglePlantById(int id) async {
    final plantDao = await _plantDao;
    final dbPlant = await plantDao.findPlantById(id);
    return dbPlant?.plant;
  }

  Future<List<Plant>> getPlantList() async {
    final plantDao = await _plantDao;
    final dbPlantList = await plantDao.findAllPlant();
    return dbPlantList.map((p) => p.plant).toList();
  }
}

@dao
abstract class PlantDao {
  @Query('SELECT * from DBPlant')
  Future<List<DBPlant>> findAllPlant();

  @Query('SELECT * from DBPlant WHERE id = :id')
  Future<DBPlant?> findPlantById(int id);

  @Query('SELECT * from DBPlant WHERE name = :name')
  Future<DBPlant?> findPlantByName(String name);

  @insert
  Future<void> insertPlant(DBPlant plant);

  @Query(
      'UPDATE DBPlant SET name = :name, description = :description, quantity = :quantity WHERE id = :id')
  Future<void> updatePlant(
      String name, String description, int quantity, int id);

  @Query('DELETE from DBPlant WHERE id = :id')
  Future<DBPlant?> deletePlant(int id);
}

@entity
class DBPlant {
  @PrimaryKey(autoGenerate: true)
  int? id;
  String name;
  String description;
  int quantity;
  String publishDate;
  String imageUrl;

  // constructor for DB with id autogenerate
  DBPlant(this.id, this.name, this.description, this.quantity, this.publishDate,
      this.imageUrl);
  //this.image);

  // the constructor for us in the app
  DBPlant.fromMap(Plant plant)
      : id = plant.id,
        name = plant.name,
        description = plant.description,
        quantity = plant.quantity,
        publishDate = plant.date.toString(),
        imageUrl = plant.imageUrl;

  //recreate a plant object with DBPlant values
  Plant get plant => Plant(id, name, description, quantity, imageUrl);
  //imageFromBase64String(image));

}
