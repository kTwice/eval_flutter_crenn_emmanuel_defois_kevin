import 'package:flutter/material.dart';
import 'package:im_plante_e/model/plant_manager.dart';
import 'package:im_plante_e/view/home_page.dart';

import 'update_plant_page.dart';

class SinglePlantPage extends StatelessWidget {
  final plantManager = PlantManager();
  final plant;
  SinglePlantPage({super.key, this.plant});

  @override
  Widget build(BuildContext context) {
    const appName = "im_plante_e";
    return Scaffold(
        appBar: AppBar(
            title: const Text(
          appName,
          textAlign: TextAlign.center,
        )),
        body: SafeArea(
            child: Center(
                child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.network(plant.imageUrl, fit: BoxFit.fill, width: 150, height: 150,),
                  ],

                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      plant!.name,
                      style: Theme.of(context).textTheme.headline4,
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    const Text("quantité : "),
                    Text(plant!.quantity.toString()),
                    const SizedBox(
                      width: 12,
                    ),
                  ],
                )
              ],
            ),
            Container(
              padding: const EdgeInsets.fromLTRB(10, 30, 10, 10),
              height: 260,
              width: double.maxFinite,
              child: Card(
                  elevation: 5,
                  child: Padding(
                      padding: const EdgeInsets.all(7),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [Text(plant!.description)],
                      ))),
            ),
            Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
              ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => UpdatePlantPage(plant: plant)));
                  },
                  child: const Text("Modifier")),
              ElevatedButton(
                  onPressed: () {
                    plantManager.deletePlantToLocal(plant);
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => const HomePage()));
                  },
                  child: const Text("Supprimer")),
            ]),
          ],
        ))));
  }
}
