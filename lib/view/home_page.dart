import 'package:flutter/material.dart';
import 'package:im_plante_e/model/plant_manager.dart';
import 'package:im_plante_e/view/add_plant_page.dart';
import 'package:im_plante_e/view/single_plant_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final plantManager = PlantManager();
  final _formKey = GlobalKey<FormState>();
  String _searchField = "";

  void _changeSearchFieldValue(value) {
    setState(() {
      _searchField = value;
    });
  }

  Future<void> _validateField() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      await plantManager.searchPlantByNameFromLocal(_searchField).then((plant) {
        if (plant != null) {
          (Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => SinglePlantPage(plant: plant))));
        } else {
          const Text("désolé la plante recherché n'existe pas");
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    const appName = "im_plante_e";
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            appName,
            textAlign: TextAlign.center,
          ),
          leading: IconButton(
            icon: const Icon(Icons.menu),
            onPressed: () {},
          ),
        ),
        body: SafeArea(
            child: Center(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
              Form(
                key: _formKey,
                child: Row(
                  children: [
                    const Icon(Icons.search),
                    Expanded(
                        child: TextFormField(
                      decoration: const InputDecoration(
                          helperText: "Entrez le nom de la plante recherchée",
                          hintText: "Plante"),
                      autocorrect: false,
                      textCapitalization: TextCapitalization.words,
                      validator: (value) =>
                          value!.length > 2 ? null : "nom de plante trop court",
                      onChanged: _changeSearchFieldValue,
                    )),
                    IconButton(
                        onPressed: _validateField,
                        icon: const Icon(Icons.search))
                  ],
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              FutureBuilder(
                  future: plantManager.loadPlantListFromLocal(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return Expanded(
                          child: GridView.builder(
                              itemCount: plantManager.plants.length,
                              gridDelegate:
                                  const SliverGridDelegateWithMaxCrossAxisExtent(
                                      maxCrossAxisExtent: 200,
                                      childAspectRatio: 3 / 2,
                                      crossAxisSpacing: 20,
                                      mainAxisSpacing: 20),
                              itemBuilder: _streamList));
                    } else {
                      return const CircularProgressIndicator();
                    }
                  }),
              const SizedBox(
                height: 16,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => const AddPlantPage()));
                      },
                      child: const Text("Ajouter une plante"))
                ],
              )
            ]))));
  }

  Widget _streamList(BuildContext context, int rowNumber) {
    final plant = plantManager.plants[rowNumber];
    return
    InkWell(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => SinglePlantPage(plant: plant)));
      },
      child :Card(
      child: Column(
        children: [
          Flexible(child:
          Image.network(plant.imageUrl, fit: BoxFit.fill, width: 150, height: 150,),),
          Container(
              color: Colors.white,
              child: Column(
                children: [
                  Text(plant.name, style: const TextStyle(fontWeight: FontWeight.bold),),
                ],
              )

          )
        ],
      ),
    ));
  }
}
