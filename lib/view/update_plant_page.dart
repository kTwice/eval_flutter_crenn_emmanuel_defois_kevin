import 'package:flutter/material.dart';
import 'package:im_plante_e/model/plant.dart';
import 'package:im_plante_e/model/plant_manager.dart';
import 'package:im_plante_e/view/home_page.dart';

class UpdatePlantPage extends StatefulWidget {
  final Plant plant;
  const UpdatePlantPage({super.key, required this.plant});

  @override
  State<UpdatePlantPage> createState() => _UpdatePlantPageState();
}

class _UpdatePlantPageState extends State<UpdatePlantPage> {
  final plantManager = PlantManager();
  final _formKey = GlobalKey<FormState>();
  String _plantName = "";
  String _plantUrl = "";
  int _quantity = 0;
  String _comment = "";

  void _changeName(value) {
    setState(() {
      _plantName = value;
    });
  }

  void _changeQuantity(value) {
    setState(() {
      _quantity = int.tryParse(value)!;
    });
  }

  void _changeComment(value) {
    setState(() {
      _comment = value;
    });
  }

  void _changeUrl(value) {
    setState(() {
      _plantUrl = value;
    });
  }

  void _validateFields() {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      plantManager.changePlantToLocal(
          widget.plant, _plantName, _quantity, _comment, _plantUrl);
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => const HomePage()));
    }
  }

  @override
  Widget build(BuildContext context) {
    const appName = "im_plante_e";
    return Scaffold(
        appBar: AppBar(
            title: const Text(
          appName,
          textAlign: TextAlign.center,
        )),
        body: SafeArea(
            child: Center(
                child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    widget.plant.name,
                    style: Theme.of(context).textTheme.headline4,
                  ),
                ],
              ),
              Row(
                children: [
                  const Icon(Icons.title),
                  Expanded(
                    child: TextFormField(
                      initialValue: widget.plant.name,
                      decoration: const InputDecoration(
                          helperText: "Entrez le nom de la plante",
                          hintText: "plante"),
                      autocorrect: false,
                      textCapitalization: TextCapitalization.words,
                      autofillHints: const [AutofillHints.givenName],
                      keyboardType: TextInputType.name,
                      validator: (value) =>
                          value!.length > 2 ? null : "nom de plante trop court",
                      onSaved: _changeName,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  const Icon(Icons.numbers),
                  Expanded(
                    child: TextFormField(
                      initialValue: widget.plant.quantity.toString(),
                      decoration: const InputDecoration(
                        helperText: "Selectionner la quantité",
                      ),
                      autocorrect: false,
                      validator: (value) =>
                          value != null ? null : "veuillez inscrire un nombre",
                      onSaved: _changeQuantity,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  const Icon(Icons.text_fields),
                  Expanded(
                    child: TextFormField(
                      initialValue: widget.plant.description,
                      keyboardType: TextInputType.multiline,
                      maxLines: 4,
                      decoration: const InputDecoration(
                        helperText: "ajouter une description",
                      ),
                      autocorrect: false,
                      validator: (value) => value != null
                          ? null
                          : "veuillez saisir une description",
                      onSaved: _changeComment,
                    ),
                  ),
                ],
              ),
              Row(children: [
                const Icon(Icons.photo),
                Expanded(
                  child: TextFormField(
                    initialValue: widget.plant.imageUrl,
                    decoration: const InputDecoration(
                        helperText: "Entrez l'url de la photo de la plante",
                        hintText: "Url de la photo"),
                    autocorrect: false,
                    keyboardType: TextInputType.name,
                    validator: (value) =>
                        value!.length > 2 ? null : "veuillez saisir l'url",
                    onSaved: _changeUrl,
                  ),
                ),
              ]),
              ElevatedButton(
                  onPressed: _validateFields, child: const Text("Valider")),
            ],
          ),
        ))));
  }
}
